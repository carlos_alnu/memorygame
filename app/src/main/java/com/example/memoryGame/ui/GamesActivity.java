package com.example.memoryGame.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

import com.example.memoryGame.R;

public class GamesActivity extends AppCompatActivity {

    private Button btnG1;
    private Button btnG2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);

        String listGames [] = {"Normal Game" , "Vs Time"};

        btnG1 = (Button) findViewById(R.id.btnGame1);
        btnG1.setText(listGames[0]);
        btnG2 = (Button) findViewById(R.id.btnGame2);
        btnG2.setText(listGames[1]);
    }
}
